#!/usr/bin/env python
import sys
import platform
import json
import datetime
import pymongo
import time
#http://chimera.labs.oreilly.com/books/1230000000393/ch06.html#_solution_104
#
SLEEP_TIME = 10
HOST_NAME = '192.168.0.2'
DB_NAME = 'bg_db'

connection = pymongo.Connection(host=HOST_NAME)
admin_db = connection[DB_NAME]

info = {}

info['serverStatus'] = admin_db.command("serverStatus")
info['isMaster'] = admin_db.command("isMaster")
# info['connPoolStats'] = admin_db.command("connPoolStats")
# info['cursorInfo'] = admin_db.command("cursorInfo")
info['dbStats'] = admin_db.command("dbStats")
info['whatsmyuri'] = admin_db.command("whatsmyuri")
# info['getCmdLineOpts'] = admin_db.command("getCmdLineOpts")

if 'members' in info['isMaster']:
    info['replSetGetStatus'] = admin_db.command("replSetGetStatus")

dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None

while True:
	# print json.dumps(info, sort_keys=True, indent=2, default=dthandler)
	print info.values()
	time.sleep(SLEEP_TIME)

{'whatsmyuri': {u'you': u'192.168.0.8:49746', u'ok': 1.0}, 'isMaster': {u'ismaster': True, u'maxBsonObjectSize': 16777216, u'ok': 1.0, u'maxMessageSizeBytes': 48000000, u'localTime': datetime.datetime(2013, 11, 1, 17, 16, 41, 770000)}, 'serverStatus': {u'metrics': {u'getLastError': {u'wtime': {u'num': 43, u'totalMillis': 0}, u'wtimeouts': 0L}, u'queryExecutor': {u'scanned': 1006076934L}, u'record': {u'moves': 59587L}, u'repl': {u'buffer': {u'count': 0L, u'sizeBytes': 0L, u'maxSizeBytes': 268435456}, u'apply': {u'batches': {u'num': 0, u'totalMillis': 0}, u'ops': 0L}, u'oplog': {u'insert': {u'num': 0, u'totalMillis': 0}, u'insertBytes': 0L}, u'network': {u'bytes': 0L, u'readersCreated': 0L, u'getmores': {u'num': 0, u'totalMillis': 0}, u'ops': 0L}, u'preload': {u'docs': {u'num': 0, u'totalMillis': 0}, u'indexes': {u'num': 0, u'totalMillis': 0}}}, u'ttl': {u'passes': 584L, u'deletedDocuments': 0L}, u'operation': {u'fastmod': 500000L, u'scanAndOrder': 9310L, u'idhack': 1518617L}, u'document': {u'deleted': 0L, u'updated': 1502737L, u'inserted': 1050044L, u'returned': 276184L}}, u'process': u'mongod', u'pid': 3280, u'connections': {u'current': 6, u'available': 198, u'totalCreated': 176L}, u'locks': {u'admin': {u'timeAcquiringMicros': {u'r': 5403L, u'w': 0L}, u'timeLockedMicros': {u'r': 103101L, u'w': 0L}}, u'config': {u'timeAcquiringMicros': {u'r': 41277L, u'w': 0L}, u'timeLockedMicros': {u'r': 90422L, u'w': 0L}}, u'bg_local': {u'timeAcquiringMicros': {u'r': 7097445707L, u'w': 1236617702L}, u'timeLockedMicros': {u'r': 14484655123L, u'w': 217528019L}}, u'.': {u'timeAcquiringMicros': {u'R': 25663833L, u'W': 3549499L}, u'timeLockedMicros': {u'R': 5672039L, u'W': 70053539L}}, u'bg_db': {u'timeAcquiringMicros': {u'r': 1138L, u'w': 0L}, u'timeLockedMicros': {u'r': 1593L, u'w': 0L}}, u'local': {u'timeAcquiringMicros': {u'r': 9107994L, u'w': 0L}, u'timeLockedMicros': {u'r': 411755L, u'w': 0L}}, u'local0': {u'timeAcquiringMicros': {u'r': 61683L, u'w': 0L}, u'timeLockedMicros': {u'r': 179285L, u'w': 0L}}}, u'cursors': {u'clientCursors_size': 0, u'timedOut': 8, u'totalOpen': 0}, u'globalLock': {u'totalTime': 63397720000L, u'lockTime': 70053539L, u'currentQueue': {u'total': 0, u'writers': 0, u'readers': 0}, u'activeClients': {u'total': 0, u'writers': 0, u'readers': 0}}, u'extra_info': {u'note': u'fields vary by platform', u'page_faults': 116030}, u'uptime': 63397.0, u'network': {u'numRequests': 4293805, u'bytesOut': 574360424, u'bytesIn': 720641490}, u'uptimeMillis': 63397720L, u'recordStats': {u'admin': {u'pageFaultExceptionsThrown': 0, u'accessesNotInMemory': 0}, u'local': {u'pageFaultExceptionsThrown': 0, u'accessesNotInMemory': 0}, u'bg_local': {u'pageFaultExceptionsThrown': 0, u'accessesNotInMemory': 57}, u'pageFaultExceptionsThrown': 0, u'bg_db': {u'pageFaultExceptionsThrown': 0, u'accessesNotInMemory': 0}, u'config': {u'pageFaultExceptionsThrown': 0, u'accessesNotInMemory': 0}, u'local0': {u'pageFaultExceptionsThrown': 0, u'accessesNotInMemory': 0}, u'accessesNotInMemory': 57}, u'version': u'2.4.6', u'dur': {u'compression': 0.0, u'journaledMB': 0.0, u'commits': 29, u'writeToDataFilesMB': 0.0, u'commitsInWriteLock': 0, u'earlyCommits': 0, u'timeMs': {u'writeToJournal': 0, u'dt': 3016, u'remapPrivateView': 0, u'prepLogBuffer': 0, u'writeToDataFiles': 0}}, u'mem': {u'resident': 976, u'supported': True, u'virtual': 5009, u'mappedWithJournal': 2528, u'mapped': 1264, u'bits': 64}, u'opcountersRepl': {u'getmore': 0, u'insert': 0, u'update': 0, u'command': 0, u'query': 0, u'delete': 0}, u'indexCounters': {u'missRatio': 0.0, u'resets': 0, u'hits': 9373058, u'misses': 0, u'accesses': 9373070}, u'uptimeEstimate': 32259.0, u'host': u'iMacus.local', u'writeBacksQueued': False, u'localTime': datetime.datetime(2013, 11, 1, 17, 16, 41, 765000), u'backgroundFlushing': {u'last_finished': datetime.datetime(2013, 11, 1, 17, 16, 28, 576000), u'last_ms': 116, u'flushes': 585, u'average_ms': 48600.23931623931, u'total_ms': 28431140}, u'opcounters': {u'getmore': 0, u'insert': 1050044, u'update': 1502737, u'command': 1598601, u'query': 144366, u'delete': 0}, u'ok': 1.0, u'asserts': {u'msg': 0, u'rollovers': 0, u'regular': 0, u'warning': 0, u'user': 0}}, 'dbStats': {u'storageSize': 0, u'ok': 1.0, u'avgObjSize': 0.0, u'dataFileVersion': {}, u'db': u'bg_db', u'indexes': 0, u'objects': 0, u'collections': 0, u'fileSize': 0, u'numExtents': 0, u'dataSize': 0, u'indexSize': 0, u'nsSizeMB': 0}}

test_array.append({'fruit': 'apple', 'quantity': 5, 'color': 'red'});
test_array.append({'fruit': 'pear', 'quantity': 8, 'color': 'green'});
test_array.append({'fruit': 'banana', 'quantity': 3, 'color': 'yellow'});
test_array.append({'fruit': 'orange', 'quantity': 11, 'color': 'orange'});
fieldnames = ['fruit', 'quantity', 'color']
test_file = open('test2.csv','wb')
csvwriter = csv.DictWriter(test_file, delimiter=',', fieldnames=fieldnames)
csvwriter.writerow(dict((fn,fn) for fn in fieldnames))
for row in test_array:
     csvwriter.writerow(row)
test_file.close()



# print sys.platform
# print platform.machine()
# print platform.node()
# print platform.processor()
# print platform.system()
# print platform.uname()

# # Platform specific
# print platform.win32_ver()
# print platform.mac_ver()
# print platform.linux_distribution()


